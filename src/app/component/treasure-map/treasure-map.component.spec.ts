import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TreasureMapComponent} from './treasure-map.component';
import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from "@angular/core";
import {fakeFileListPlainText} from "./treasure-map.component.mock";
import {InputFileComponent} from "../input-file/input-file.component";

describe('TreasureMapComponent', () => {
  let treasureMapComponent: TreasureMapComponent;
  let inputFileComponent: InputFileComponent;
  let fixture: ComponentFixture<TreasureMapComponent>;
  let mockElementRef = {uploadedFile: {nativeElement: {value: ''}}};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TreasureMapComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [TreasureMapComponent, {
        provide: InputFileComponent,
        useValue: inputFileComponent
      }, {provide: InputFileComponent, useValue: mockElementRef}]
    })
      .compileComponents();
  });

  beforeEach(() => {

    fixture = TestBed.createComponent(TreasureMapComponent);
    treasureMapComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the map', () => {

    expect(treasureMapComponent).toBeTruthy();
  });

  it('should call readAsText() if file input type is plain text file', () => {
    const fileReaderSpy: FileReader = jasmine.createSpyObj('FileReader', ['readAsText', 'onload']);
    spyOn(window as any, 'FileReader').and.returnValue(fileReaderSpy);
    spyOn(treasureMapComponent, 'reset');

    treasureMapComponent.readFile(fakeFileListPlainText);

    expect(fileReaderSpy.readAsText).toHaveBeenCalled();
    expect(fileReaderSpy.onload).toHaveBeenCalled();

  });

  it('should call formatInputFile() if file input type is plain text file', () => {
    const fileReaderSpy: FileReader = jasmine.createSpyObj('FileReader', ['readAsText', 'onload']);
    spyOn(treasureMapComponent, 'formatInputFile');
    spyOn(window as any, 'FileReader').and.returnValue(fileReaderSpy);

    treasureMapComponent.readFile(fakeFileListPlainText);

    expect(treasureMapComponent.formatInputFile).toHaveBeenCalledWith('test-file\n-input');
  });

})
