import {AfterViewInit, Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {InputFileComponent} from "../input-file/input-file.component";

@Component({
  selector: 'app-treasure-map',
  templateUrl: './treasure-map.component.html',
  styleUrls: ['./treasure-map.component.scss']
})
export class TreasureMapComponent implements OnInit {
  @ViewChild('uploadedRef', {static: false}) uploadedRef: InputFileComponent;

  public fileString: string = "";
  public fileFormated: string[] = [];
  public newMap: string[][] = [];
  public isOutOfMap: boolean = false;
  public errorMessage: string = '';
  public errorTimer: number;
  public uploadRef: ElementRef;
  public file: any;

  constructor() {
  }

  ngOnInit(): void {
  }

  // read input value from Input component, processed by FileReader object and formatted by formatInputFile method
  public readFile(inputValue: any) {
    let file: File = inputValue;
    let myReader: FileReader = new FileReader();
    clearTimeout(this.errorTimer);
    if (file.type !== "text/plain") {
      this.reset();
      this.errorMessage = 'Votre fichier doit être un fichier text (.txt)'
      console.log("this.errorMessage", this.errorMessage)

      this.errorTimer = setTimeout(() => {
        this.errorMessage = '';
      }, 3000);
    } else {
      myReader.onload = async () => {
        try {
          this.fileString = await myReader.result as string;
          this.formatInputFile(this.fileString)
        } catch (error) {
          this.reset();
          console.log("Invalid file format", error);
        }
      }
      myReader.readAsText(file);
    }
  }
// split file given in param, get array for each values contains elements to create map
  public formatInputFile = (file: string) => {
    this.fileFormated = file.split(/\r?\n/);
    this.fileFormated = this.fileFormated.filter(elem => !elem.startsWith('#'))

    let newMapSize: string[] = this.fileFormated.filter(elem => elem.startsWith('C'))
    let mountainsAxes: string[] = this.fileFormated.filter(elem => elem.startsWith('M'))
    let treasureAxes: string[] = this.fileFormated.filter(elem => elem.startsWith('T'))
    let adventurerAxes: string[] = this.fileFormated.filter(elem => elem.startsWith('A'))

    this.createMap(newMapSize, mountainsAxes, treasureAxes, adventurerAxes);
  }


  public createMap = (newMap: string[], mountains: string[], treasure: string[], adventurer: string[]) => {
    if (newMap) {
      newMap.forEach(elem => {
        if (elem) {
          let newMapConfig = elem.split('-')
          let rows = Number(newMapConfig[1]);
          let cols = Number(newMapConfig[2]);

          this.newMap = new Array(cols).fill('.').map(() => new Array(rows).fill(('.')));
        }
      })
    }
    this.mapElementsAxesConfig(mountains, treasure);
    this.adventurerAxesConfig(adventurer);
  }

  // position the elements, catch the rows & cols values for mountains & treasure
  public mapElementsAxesConfig = (mountains: string[], treasure: string[]) => {
    if (mountains) {
      mountains.forEach(elem => {
        if (elem) {
          let mountainsSplited = elem.split('-')
          let horizontalAxe = Number(mountainsSplited[1])
          let verticalAxe = Number(mountainsSplited[2])

          this.checkEndOfMap(this.newMap[0].length, horizontalAxe, mountainsSplited[0])
          this.newMap[verticalAxe][horizontalAxe] = mountainsSplited[0]
        }
      })
    }
    if (treasure) {
      treasure.forEach(elem => {
        if (elem) {
          let treasureSplited = elem.split('-')
          let horizontalAxe = Number(treasureSplited[1])
          let verticalAxe = Number(treasureSplited[2])

          this.checkEndOfMap(this.newMap[0].length, horizontalAxe, treasureSplited[0])
          this.newMap[verticalAxe][horizontalAxe] = treasureSplited[0] + (`(${treasureSplited[3]})`)
        }
      })
    }
  }

  //check end of the map, base on the value in the instruction file, if rows's element  is > to the size map, return an error
  public checkEndOfMap = (newMap: number, rowPosition: number, wrongAxesConfig: string) => {

    this.isOutOfMap = (rowPosition > (newMap - 1));

    if (this.isOutOfMap) {
      this.errorTimer = setTimeout(() => {
        this.errorMessage = '';
      }, 3000);
      this.errorMessage = `Out of the map for one of the ${wrongAxesConfig} , please check this out`;
      this.reset();
    }
  }
//position the adventurer on the map, base on the value in the instruction file
  public adventurerAxesConfig = (adventurer: string[]) => {
    if (adventurer) {
      adventurer.forEach(elem => {
        if (elem) {
          let adventurerSplited = elem.split('-')
          let horizontalAxe = Number(adventurerSplited[2])
          let verticalAxe = Number(adventurerSplited[3])

          this.checkEndOfMap(this.newMap[0].length, horizontalAxe, adventurerSplited[0])
          this.newMap[verticalAxe][horizontalAxe] = adventurerSplited[0]
        }
      })
    }
  }

  public reset = () => {
    this.uploadedRef.uploadedFile.nativeElement.value = '';
    this.newMap = [];
    this.fileString = '';
    this.fileFormated = [];
  }

}
