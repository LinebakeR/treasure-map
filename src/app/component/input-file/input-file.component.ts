import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-input-file',
  templateUrl: './input-file.component.html',
  styleUrls: ['./input-file.component.scss']
})
export class InputFileComponent {
  @ViewChild('uploadedFile', {static: false}) uploadedFile: ElementRef;

  public fileSendToParent: FileList

  // Uploaded file send to TreasureMapComponent
  @Output() public fileUploaded: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  // Catching the value on input of type: File
  public onFileChange = (event: any) => {
    if (event.target.files.length > 0) {
      this.fileSendToParent = event.target.files[0];
      this.fileUploaded.emit(this.fileSendToParent);
    }
  }

}
