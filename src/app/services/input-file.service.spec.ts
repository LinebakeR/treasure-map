import { TestBed } from '@angular/core/testing';

import { InputFileService } from './input-file.service';

describe('InputFileService', () => {
  let service: InputFileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InputFileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
